package com.jannetta.chaosgenome.model;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class GLCMTest {

	GLCM glcm = new GLCM();
	int[][] imagematrix = new int[][] {
			{0, 0, 2, 2, 3},
			{0, 0, 2, 2, 3},
			{0, 0, 0, 0, 4},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0}
			};
	Coord[] vertices = new Coord[4];
	long[][] matrix;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		int greylevels = 4;
		vertices[0] = new Coord(0,0);
		vertices[1] = new Coord(5,0);
		vertices[2] = new Coord(5,5);
		vertices[3] = new Coord(0,5);
		GLCM.initGLCM(imagematrix, vertices, greylevels);
		GLCM.calcGLCM();
		matrix = glcm.getMatrix();
		System.out.println("Image");
		for (int row = 0;row < imagematrix.length; row++) {
			for (int column = 0; column < imagematrix.length; column++) {
				System.out.print(imagematrix[row][column]);
			}
			System.out.println();
		}
		System.out.println("GLCM");
		for (int row = 0; row < matrix.length; row++) {
			for (int column = 0; column < matrix.length; column++) {
				System.out.print(matrix[row][column] + " ");
			}
			System.out.println();
		}
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}

}