package com.jannetta.chaosgenome.controller;
import static org.junit.Assert.*;
import com.jannetta.chaosgenome.model.Coord;

import org.junit.Before;
import org.junit.Test;


public class HelpersTest {
	float[][] vertex = new float[5][2];
	Coord[] vert = new Coord[5];
	@Before
	public void setUp() {
		vertex[0][0] = 550;
		vertex[0][1] = 450;
		vertex[1][0] = 455;
		vertex[1][1] = 519;
		vertex[2][0] = 491;
		vertex[2][1] = 631;
		vertex[3][0] = 609;
		vertex[3][1] = 631;
		vertex[4][0] = 645;
		vertex[4][1] = 519;
		vert[0] = new Coord(550,450);
		vert[1] = new Coord(455,519);
		vert[2] = new Coord(491,631);
		vert[3] = new Coord(609,631);
		vert[4] = new Coord(645,519);
	}

	@Test
	public void test1() {
		assertFalse(Helpers.isPointInPath(4, 20, vertex));
	}

	@Test
	public void test2() {
		assertTrue(Helpers.isPointInPath(550, 456, vertex));
	}

	@Test
	public void test3() {
		assertFalse(Helpers.isPointInPath(456, 500, vertex));
	}

	@Test
	public void test4() {
		assertFalse(Helpers.isPointInPath2(4, 20, vert));
	}

	@Test
	public void test5() {
		assertTrue(Helpers.isPointInPath2(550, 456, vert));
	}

	@Test
	public void test6() {
		assertFalse(Helpers.isPointInPath2(456, 500, vert));
	}

}