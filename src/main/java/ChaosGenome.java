

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.jannetta.chaosgenome.view.MainWindow;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class ChaosGenome extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new MainWindow();
			}
		});
	}
}
