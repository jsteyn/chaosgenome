package com.jannetta.chaosgenome.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class VertexGen {

	private int vertex;
	/**
	 * Number of vertices required for polygon
	 */
	private int numberOfVertices;
	
	/**
	 * Number of point for which random number are to be generated or in the case of reading from a file
	 * this will be the number of nucleotides, codons or amino acids in the file.
	 */
	private long numberOfPoints;
	
	/**
	 * Name of file to read from 
	 */
	private String filename;
	
	/**
	 * Boolean indicating whether vertex is to be generated randomly or read from file
	 */
	private boolean fromfile = false;
	
	/**
	 * Scanner used to read from file
	 */
	private Scanner sc;
	
	/**
	 * String for holding line read from file
	 */
	String line;
	
	/**
	 * 
	 */
	ArrayList<String> chars = new ArrayList<String>();
	Hashtable<String, Integer> vertices = new Hashtable<String, Integer>();
	int index = 0;

	
	/**
	 * Generate random point polygon
	 * @param numberOfVertices Number of vertices for polygon
	 * @param numberOfPoints Number of vertices to be generated to fill polygon
	 */
	public VertexGen(int numberOfVertices, long numberOfPoints) {
		if (numberOfVertices < 3) numberOfVertices = 3;
		if (numberOfPoints < 1000) numberOfPoints = 1000;
		this.numberOfVertices = numberOfVertices;
		this.filename = null;
		generateVertices();
	}

	/**
	 * Generate polygon from file
	 * @param filename
	 */
	public VertexGen(String filename) {
		this.filename = filename;
		generateVertices();
	}

	final public void generateVertices() {
		numberOfPoints = 0;
		fromfile = !(this.filename == null || filename.equals(""));
		if (!fromfile) {
			System.out.println("No filename, generating vertices randomly");
		} else {
			System.out.println("Reading file: " + filename);
			try {
				sc = new Scanner(new File(filename));
				// read header line from file
				line = sc.nextLine();
				// read rest of file
				while (sc.hasNextLine()) {
					// read line from file
					line = sc.nextLine();
					// calculate length of line read
					int l = line.length();
					for (int i = 0; i < l; i++) {
						String keyval = "" + line.charAt(i);
						// read one character at a time from string and add to array
						chars.add(keyval);
						// for each character read add one to number of points
						numberOfPoints++;
						// add vertex to list of vertices where the key is the character and the value an incrementing value giving
						// each key a unique value that can later be used for lookup purposes, eg.
						// A = 0, T = 1, G = 2, C = 3
						if (!vertices.containsKey(keyval)) {
							vertices.put(keyval, vertices.size());
						}
					}
				}
				System.out.println("Number of points read: " + numberOfPoints);
				sc.close();
				//vertices.keys();
				// Set the number of vertices for this polygon. Will usually be 4 for a file containing nucleotides
				// 21 for amino acids and 64 for codons
				this.numberOfVertices = vertices.size();
			} catch (IOException e) {
				//
			}
		}
	}

	/**
	 * Get a vertex, if not from file then generate randomly, else get next character from file that determines the vector
	 * @return vertex
	 */
	public int getVertex() {
		if (fromfile) {
			String get = chars.get(index);
			vertex = vertices.get(get);
			index++;
		} else {
			vertex = (int) (Math.random() * (numberOfVertices));
		}
		return vertex;

	}

	/**
	 * Returns the number of vertices for  the polygon
	 * 
	 * @return
	 */
	public int getNumberOfVertices() {
		return this.numberOfVertices;

	}
	
	/**
	 * Returns the number of points in the images, or nucleotides, amino acids or codons from the file
	 * @return
	 */
	public long getNumberOfPoints() {
		return this.numberOfPoints;
	}
}
