package com.jannetta.chaosgenome.controller;

import com.jannetta.chaosgenome.model.Coord;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class DrawMatrix {

	int size;
	float[][] matrix;
	long numberOfPoints;
	int numberOfVertices;
	float radius;
	private float startAng = 0;
	private float centerAng = 0;
	private int cx = 0, cy = 0;
	private Coord[] vertex;
	private Coord c_curr, plot_point;
	private float maxval = 0;
	private VertexGen vg;
	private int maxgreylevel;

	public DrawMatrix() {
	}

	/**
	 * Draw a polygon generating the values randomly
	 * 
	 * @param numberOfPoints
	 * @param numberOfVertices
	 * @param radius
	 * @param size
	 *            From : http://www.mathopenref.com/coordpolycalc.html
	 */
	final public void drawRandom(long numberOfPoints, int numberOfVertices,
			float radius, int size) {
		this.radius = radius;
		this.size = size;
		this.numberOfVertices = numberOfVertices;
		this.numberOfPoints = numberOfPoints;
		this.matrix = new float[size][size];
		initMatrix();
		centerAng = (float) (2 * Math.PI / numberOfVertices);
		vg = new VertexGen(numberOfVertices, numberOfPoints);
		this.numberOfVertices = vg.getNumberOfVertices();
		System.out.println("Number of points: " + this.numberOfPoints);
		vertex = new Coord[numberOfVertices];
		initVertices();
		plot_point = new Coord(size / 2, size / 2);
		for (long points = 0; points < numberOfPoints; points++) {
			int v = vg.getVertex();
			for (int j = 0; j < numberOfVertices; j++) {
				if (v == j) {
					c_curr = vertex[j];
				}
			}
			plot_point = new Coord((plot_point.getX() + c_curr.getX()) / 2,
					(plot_point.getY() + c_curr.getY()) / 2);
			// Increment the value of the determined point
			setMatrixPoint(
					(int) plot_point.getX(),
					(int) plot_point.getY(),
					getMatrixPoint((int) plot_point.getX(),
							(int) plot_point.getY()) + 1);
			calcMaxVal(matrix[(int) plot_point.getX()][(int) plot_point.getY()]);
		}
	}

	/**
	 * Draw a polygon reading the values from file
	 * 
	 * @param filename
	 * @param radius
	 * @param size
	 */
	final public void drawFile(String filename, float radius, int size, int grey) {
		this.size = size;
		this.matrix = new float[size][size];
		initMatrix();
		vg = new VertexGen(filename);
		this.numberOfVertices = vg.getNumberOfVertices();
		this.numberOfPoints = vg.getNumberOfPoints();
		this.maxgreylevel = grey;
		this.centerAng = (float) (2 * Math.PI / this.numberOfVertices);
		this.vertex = new Coord[this.numberOfVertices];
		this.radius = radius;
		initVertices();
		plot_point = new Coord(size / 2, size / 2);
		for (long points = 0; points < numberOfPoints; points++) {
			int v = vg.getVertex();
			for (int j = 0; j < numberOfVertices; j++) {
				if (v == j) {
					c_curr = vertex[j];
				}
			}
			plot_point = new Coord((plot_point.getX() + c_curr.getX()) / 2,
					(plot_point.getY() + c_curr.getY()) / 2);
			// Increment the value of the determined point
			setMatrixPoint((int)plot_point.getX(), (int)plot_point.getY(), getMatrixPoint((int)plot_point.getX(), (int)plot_point.getY())+1);
			calcMaxVal(matrix[(int) plot_point.getX()][(int) plot_point.getY()]);
		}
	}

	public void setMatrixPoint(int x, int y, float value) {
		matrix[x][y] = value;
	}

	public void incMatrixPoint(int x, int y) {
		matrix[x][y]++;
	}

	public float getMatrixPoint(int x, int y) {
		return matrix[x][y];
	}

	public float[][] getMatrix() {
		return matrix;		
	}
	
	public int[][] getIntMatrix() {
		int[][] intmatrix = new int[matrix.length][matrix.length];
		for (int row = 0; row < matrix.length; row++) {
			for (int column = 0; column < matrix.length; column++) {
				intmatrix[row][row] = (int) matrix[row][row];
			}
		}
		return intmatrix;
	}

	public float getMaxval() {
		return maxval;
	}

	public Coord[] getVertices() {
		return vertex;
	}

	private void calcMaxVal(float val) {
		if (val > maxval)
			maxval = val;
	}

	private void initMatrix() {
		// initialise matrix
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				setMatrixPoint(i, j, 0);
			}
		}
	}
	
	private void initVertices() {
		System.out.println("Initialising vertices ...");
		//if (numberOfVertices % 2 == 0) {
		//	this.startAng = (float) (this.startAng + Math.PI / 2);
		//} else {
			this.startAng = (float) (this.startAng + Math.PI / 2 - this.centerAng / 2);
		//}
		// initialise vertices
		for (int i = 0; i < this.numberOfVertices; i++) {
			float angle = this.startAng + (i * this.centerAng);
			System.out.println("Angle: " + angle);
			float vx = Math.round(this.cx + this.radius * Math.cos(angle));
			float vy = Math.round(this.cy - this.radius * Math.sin(angle));
			// add half of size to move point onto plotting area
			this.vertex[i] = new Coord(vx + this.size / 2, vy + this.size / 2);
			System.out.println("Vertex " + i + ": " + vertex[i].getX() + ", " + vertex[i].getY());
		}
	}


}
