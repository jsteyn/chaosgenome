package com.jannetta.chaosgenome.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JTextArea;

import com.jannetta.chaosgenome.model.Coord;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class Helpers {

	/**
	 * From: http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/
	 * pnpoly.html http://en.wikipedia.org/wiki/Even%E2%80%93odd_rule
	 * 
	 * @param x
	 *            X-coordinate of point
	 * @param y
	 *            Y-coordinate of point
	 * @param poly
	 *            vertices of polygon
	 * @return true if point is in polygon, otherwise false
	 * 
	 */
	public static boolean isPointInPath(int x, int y, float[][] poly) {
		int num = poly.length;
		int j = num - 1;
		boolean c = false;
		for (int i = 0; i < num; i++) {
			if ((poly[i][1] > y) != (poly[j][1] > y)
					&& (x < (poly[j][0] - poly[i][0]) * (y - poly[i][1])
							/ (poly[j][1] - poly[i][1]) + poly[i][0])) {
				c = !c;
			}
			j = i;

		}
		return c;
	}

	public static boolean isPointInPath2(int x, int y, Coord[] poly) {
		int num = poly.length;
		int i, j = 0;
		boolean c = false;
		for (i = 0, j = num - 1; i < num; j = i++) {
			if (((poly[i].getY() > y) != (poly[j].getY() > y))
					&& (x < (poly[j].getX() - poly[i].getX())
							* (y - poly[i].getY())
							/ (poly[j].getY() - poly[i].getY())
							+ poly[i].getX()))
				c = !c;

		}
		return c;
	}

	public static void checkfile(String filename, JTextArea infile) {
		Scanner sc;
		try {
			sc = new Scanner(new File(filename));
			sc.nextLine(); // read header
			long linenum = 0;
			while (sc.hasNext()) {
				String line = sc.nextLine();
				for (int i = 0; i < line.length(); i++) {
					char nuc = line.charAt(i);
					//System.out.println(nuc);
					if (!(nuc == 'A' || nuc == 'T' || nuc == 'G' || nuc == 'C')) {
						// System.out.println("error on line " + linenum);
						infile.append("error on line " + linenum + " char: " + nuc + "\n");
					}
				}
				linenum++;
			}
			infile.append("\nDone checking " + filename + "\n");
			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
