package com.jannetta.chaosgenome.controller;

import java.io.File;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JTextArea;

import com.jannetta.chaosgenome.view.DrawPanel;
import com.jannetta.chaosgenome.model.AminoAcids;
import com.jannetta.chaosgenome.model.Coding;
import com.jannetta.chaosgenome.model.Codons;

/**
 * @author Jannetta S Steyn
 * Class to convert fasta nucleotide file to amino acids or codons.
 *
 */
public class ConvertFile {
	Coding coding;
	File outFileName;

	public ConvertFile(File filename, char conversion, JTextArea infile, JTextArea outfile, DrawPanel drawpanel) {
		switch (conversion) {
		case 'c': // convert to codons codes
			coding = new Codons();
			outFileName = new File(filename.getName() + ".codons.txt");
			break;
		case 'a': // convert to amino acids
			coding = new AminoAcids();
			outFileName = new File(filename.getName() + ".aa.txt");
			break;
		}
		FileWriter fw;
		int count = 0;
		try {
			if (!outFileName.exists()) {
				outFileName.createNewFile();
			}
			fw = new FileWriter(outFileName.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			Scanner sc = new Scanner(filename);
			// Read header line of fasta file
			String line = sc.nextLine();
			String tail = "";
			bw.write("\n");
			
			// Read file into text nucleotide text area
			JTextArea infileTextArea = infile;
			try {
				FileReader fri = new FileReader(filename);
				BufferedReader reader = new BufferedReader(fri);
				while ((reader.readLine()) != null) {
					infileTextArea.read(reader, "jTextArea1");
				}
			} catch (IOException ioe) {
				System.err.println(ioe);
				System.exit(1);
			}

			while (sc.hasNextLine()) {
				line = tail + sc.nextLine().trim();
				int length = (line.length() / 3) * 3;
				tail = line.substring(length);
				// Get three characters (a codon) at a time
				for (int i = 0; i < length; i += 3) {
					//
					String s = (String) coding.get(line.substring(i, i + 3));
					if (s != null) {
						bw.write(s);
						count++;
						if (count % 70 == 0)
							bw.write("\n");
					}
				}
			}
			bw.close();
			sc.close();
			// Read file into text nucleotide text area
			JTextArea outFileTextArea = outfile;
			try {
				FileReader fro = new FileReader(outFileName);
				BufferedReader reader = new BufferedReader(fro);
				while ((reader.readLine()) != null) {
					outFileTextArea.read(reader, "jTextArea1");
				}
			} catch (IOException ioe) {
				System.err.println(ioe);
				System.exit(1);
			}
			
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
