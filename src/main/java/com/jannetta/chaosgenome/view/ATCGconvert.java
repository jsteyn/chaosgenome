package com.jannetta.chaosgenome.view;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class ATCGconvert extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new MainWindow();
			}
		});
	}
}
