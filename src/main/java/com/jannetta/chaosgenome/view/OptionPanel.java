package com.jannetta.chaosgenome.view;

import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class OptionPanel extends JDialog implements ActionListener,
		PropertyChangeListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int numberOfVertices = 3;
	private JTextField tf_numberOfVertices = new JTextField(10);
	private long numberOfPoints = 1000;
	private JTextField tf_numberOfPoints = new JTextField(10);
	private float radius = 100;
	private JTextField tf_radius = new JTextField(5);
	private int maxgreylevel = 255;
	private JTextField tf_maxgreylevel = new JTextField(5);
	private String btn_enter = "Enter";
	private String btn_cancel = "Cancel";
	private MainMenuBar dd;
	private JOptionPane optionPane;
	private JPanel content = new JPanel();

	private boolean cancelled = false;

	/** Creates the reusable dialog. */
	public OptionPanel(Frame aFrame, MainMenuBar parent) {
		super(aFrame, true);
		dd = parent;
		setTitle("Polygon Parameters");
		tf_numberOfVertices.setText("3");
		tf_numberOfPoints.setText("1000000");
		tf_radius.setText("200");
		tf_maxgreylevel.setText("255");
		content.setLayout(new GridLayout(5, 1));
		content.add(new JLabel("Enter number of vertices:"));
		content.add(tf_numberOfVertices);
		content.add(new JLabel("Enter number of points:"));
		content.add(tf_numberOfPoints);
		content.add(new JLabel("Enter radius: "));
		content.add(tf_radius);
		content.add(new JLabel("Maximum grey level: "));
		content.add(tf_maxgreylevel);
		tf_numberOfVertices.addActionListener(this);
		tf_numberOfPoints.addActionListener(this);
		Object[] options = { btn_enter, btn_cancel };
		optionPane = new JOptionPane(content, JOptionPane.QUESTION_MESSAGE,
				JOptionPane.YES_NO_OPTION, null, options);
		setContentPane(optionPane);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				/*
				 * Instead of directly closing the window, we're going to change
				 * the JOptionPane's value property.
				 */
				optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
			}
		});
		optionPane.addPropertyChangeListener(this);
	}

	/** This method handles events for the text field. */
	public void actionPerformed(ActionEvent e) {
		optionPane.setValue("Enter");
	}

	/** This method reacts to state changes in the option pane. */
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();
		if (isVisible()
				&& (e.getSource() == optionPane)
				&& (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY
						.equals(prop))) {
			Object value = optionPane.getValue();

			if (value == JOptionPane.UNINITIALIZED_VALUE) {
				// ignore reset
				return;
			}

			// Reset the JOptionPane's value.
			// If you don't do this, then if the user
			// presses the same button next time, no
			// property change event will be fired.
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);
			if (value.equals("Enter")) {
				if (tf_numberOfVertices.getText().trim() != null
						|| !tf_numberOfVertices.getText().trim().equals("")) {
					Integer nov = Integer
							.valueOf(tf_numberOfVertices.getText());
					if (nov != null && nov > 2)
						numberOfVertices = nov;
					else
						numberOfVertices = 3;
				}
				if (tf_numberOfPoints.getText().trim() != null
						|| !tf_numberOfPoints.getText().trim().equals("")) {
					Long nop = Long.valueOf(tf_numberOfPoints.getText());
					if (nop != null && nop > 1000)
						numberOfPoints = nop;
					else
						numberOfPoints = 1000;
				}
				if (tf_radius.getText().trim() != null
						|| !tf_radius.getText().trim().equals("")) {
					Float rad = Float.valueOf(tf_radius.getText());
					if (rad != null && rad > 0)
						radius = rad;
					else
						radius = 100;
				}
				if (tf_maxgreylevel.getText().trim() != null
						|| !tf_maxgreylevel.getText().trim().equals("")) {
					Integer maxg = Integer.valueOf(tf_maxgreylevel.getText());
					if (maxg != null && maxg > 0)
						maxgreylevel = maxg;
					else
						maxgreylevel = 256;
				}
				cancelled = false;
				clearAndHide();
			} else if (value.equals("Cancel")) {
				dd.setLabel("Cancelled by user");
				cancelled = true;
				clearAndHide();
			} else { // user closed dialog or clicked cancel
				dd.setLabel("Cancelled by user");
				clearAndHide();
				cancelled = true;
			}
		}
	}

	/** This method clears the dialog and hides it. */
	public void clearAndHide() {
		// tf_numberOfVertices.setText(null);
		setVisible(false);
	}

	/**
	 * Returns null if the typed string was invalid; otherwise, returns the
	 * string as the user entered it.
	 */
	public int getNumberOfVertices() {

		return numberOfVertices;
	}

	public long getNumberOfPoints() {
		return numberOfPoints;
	}

	public float getRadius() {
		return radius;
	}
	
	public int getMaxGreyLevel() {
		return maxgreylevel;
	}

	public boolean isCancelled() {
		return cancelled;
	}

}
