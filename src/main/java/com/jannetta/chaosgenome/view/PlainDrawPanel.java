package com.jannetta.chaosgenome.view;

import java.awt.Graphics;

import javax.swing.JPanel;

import com.jannetta.chaosgenome.model.Coord;
import com.jannetta.chaosgenome.controller.Helpers;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 * Just a plain panel for drawing stuff on for testing purposes
 *
 */
public class PlainDrawPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	boolean draw = false;
	float[][] matrix = null;
	Coord[] vertices = null;

	@Override
	public void paintComponent(Graphics g) {
		if (matrix != null && draw)
			for (int i = 0; i < matrix[0].length; i++) {
				for (int j = 0; j < matrix[0].length; j++) {
					if (vertices != null
							&& Helpers.isPointInPath2(i, j, vertices))
						g.drawLine(i, j, i, j);
				}
			}

	}

	/**
	 * Set the vertices for the polygon and are where it is to be drawn as a matrix
	 * 
	 * @param vertices an array containing the vertices of the polygon
	 * @param matrix the matrix holding the drawing area
	 * 
	 */
	public void setVertices(Coord[] vertices, float[][] matrix) {
		this.matrix = matrix;
		draw = true;
		// initialise matrix
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				matrix[i][j] = 0;
			}
		}
		this.vertices = vertices;
		repaint();
	}

}
