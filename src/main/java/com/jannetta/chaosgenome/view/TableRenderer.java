package com.jannetta.chaosgenome.view;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.jannetta.chaosgenome.model.GLCMTableModel;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class TableRenderer extends DefaultTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	GLCMTableModel model;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		Component c = super.getTableCellRendererComponent(table, value,
				isSelected, hasFocus, row, column);
		model = (GLCMTableModel) table.getModel();
		// TODO
		return c;
	}
	
	public GLCMTableModel getGLCMTableModel() {
		return model;
	}

}
