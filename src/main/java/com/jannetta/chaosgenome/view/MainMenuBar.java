package com.jannetta.chaosgenome.view;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.jannetta.chaosgenome.model.Coord;
import com.jannetta.chaosgenome.model.GLCM;
import com.jannetta.chaosgenome.controller.ConvertFile;
import com.jannetta.chaosgenome.controller.DrawMatrix;
import com.jannetta.chaosgenome.controller.Helpers;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class MainMenuBar extends JMenuBar implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JMenu fileMenu = new JMenu("File");
	private static JMenuItem codonFile = new JMenuItem("Convert to Codons");
	private static JMenuItem aaFile = new JMenuItem("Convert to Amino Acids");
	private static JMenuItem checkFile = new JMenuItem("Check file for faults");
	private static JMenuItem exit = new JMenuItem("Exit");
	private static JMenu imageMenu = new JMenu("Image");
	private static JMenuItem produceImage = new JMenuItem("CGR from file");
	private static JMenuItem randomImage = new JMenuItem("Random CGR");
	private static JMenuItem glcmCalc = new JMenuItem("Calculate GLCM");
	private static JMenuItem saveImage = new JMenuItem("Save image");
	private static JFrame frame;
	private static String filename = "";
	private static MainWindow owner;
	private static OptionPanel optionPanel;
	private static DrawMatrix drawmatrix;
	private static int maxgreylevel;

	public MainMenuBar(MainWindow owner) {
		super();
		MainMenuBar.owner = owner;
		fileMenu.add(codonFile);
		fileMenu.add(aaFile);
		fileMenu.add(checkFile);
		fileMenu.add(exit);
		imageMenu.add(produceImage);
		imageMenu.add(randomImage);
		imageMenu.add(glcmCalc);
		imageMenu.add(saveImage);
		add(fileMenu);
		add(imageMenu);
		codonFile.addActionListener(this);
		aaFile.addActionListener(this);
		checkFile.addActionListener(this);
		exit.addActionListener(this);
		produceImage.addActionListener(this);
		randomImage.addActionListener(this);
		saveImage.addActionListener(this);
		glcmCalc.addActionListener(this);
		optionPanel = new OptionPanel(owner, this);
		optionPanel.pack();
	}

	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		if (action.equals("Convert to Codons")) {
			final JFileChooser fc = new JFileChooser(new File("."));
			int returnVal = fc.showOpenDialog(MainMenuBar.frame);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				filename = file.getName();
				new ConvertFile(new File(filename), 'c',
						owner.getNucTextArea(), owner.getConTextArea(),
						owner.getDrawPanel());
			} else {
				System.out.println("Open command cancelled by user.");
			}
		}
		if (action.equals("Convert to Amino Acids")) {
			final JFileChooser fc = new JFileChooser(new File("."));
			int returnVal = fc.showOpenDialog(MainMenuBar.frame);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				filename = file.getName();
				System.out.println(filename);
				new ConvertFile(new File(filename), 'a',
						owner.getNucTextArea(), owner.getConTextArea(),
						owner.getDrawPanel());
			} else {
				System.out.println("Open command cancelled by user.");
			}
		}
		if (action.equals("Check file for faults")) {
			final JFileChooser fc = new JFileChooser(new File("."));
			int returnVal = fc.showOpenDialog(MainMenuBar.frame);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				filename = file.getName();
				Helpers.checkfile(filename, owner.getNucTextArea());
			} else {
				System.out.println("Open command cancelled by user.");
			}

		}
		if (action.equals("Save image")) {
			owner.getDrawPanel().saveImage();
		}
		if (action.equals("Exit")) {
			System.exit(0);
		}
		if (action.equals("Random CGR")) {
			optionPanel.setLocationRelativeTo(owner);
			optionPanel.setVisible(true);
			owner.repaint();
			if (!optionPanel.isCancelled()) {
				DrawPanel drawpanel = owner.getDrawPanel();
				drawmatrix = new DrawMatrix();
				maxgreylevel = optionPanel.getMaxGreyLevel();
				drawmatrix.drawRandom(optionPanel.getNumberOfPoints(),
						optionPanel.getNumberOfVertices(),
						optionPanel.getRadius(),
						(int) (optionPanel.getRadius() * 2));
				drawpanel.drawMatrix(drawmatrix.getMatrix(),
						drawmatrix.getVertices(), maxgreylevel);
				PlainDrawPanel pdp = owner.getPlainDrawPanel();
				Coord[] v = drawmatrix.getVertices();
				float[][] dmat = new float[(int) optionPanel.getRadius() * 2][(int) optionPanel
						.getRadius() * 2];
				// initialise matrix
				for (int i = 0; i < dmat.length; i++) {
					for (int j = 0; j < dmat.length; j++) {
						dmat[i][j] = 0;
					}
				}
				pdp.setVertices(v, dmat);
			}
		}
		if (action.equals("CGR from file")) {
			final JFileChooser fc = new JFileChooser(new File("."));
			int returnVal = fc.showOpenDialog(MainMenuBar.frame);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				filename = file.getName();
				optionPanel.setLocationRelativeTo(owner);
				optionPanel.setVisible(true);
				owner.repaint();
				if (!optionPanel.isCancelled()) {
					DrawPanel drawpanel = owner.getDrawPanel();
					drawmatrix = new DrawMatrix();
					maxgreylevel = optionPanel.getMaxGreyLevel();
					drawmatrix.drawFile(filename, optionPanel.getRadius(),
							(int) (optionPanel.getRadius() * 2), maxgreylevel);
					drawpanel.drawMatrix(drawmatrix.getMatrix(),
							drawmatrix.getVertices(), maxgreylevel);

					PlainDrawPanel pdp = owner.getPlainDrawPanel();
					Coord[] v = drawmatrix.getVertices();
					float[][] dmat = new float[(int) optionPanel.getRadius() * 2][(int) optionPanel
							.getRadius() * 2];
					// initialise matrix
					for (int i = 0; i < dmat.length; i++) {
						for (int j = 0; j < dmat.length; j++) {
							dmat[i][j] = 0;
						}
					}
					pdp.setVertices(v, dmat);

				}
			}
		}
		if (action.equals("Calculate GLCM")) {
			if (drawmatrix != null) {
				GLCM.initGLCM(drawmatrix.getIntMatrix(),
						drawmatrix.getVertices(), maxgreylevel);
				GLCM.calcGLCM();
				owner.getGLCMTable().fireTableDataChanged();
				fitToContentWidth(owner.getGLCMTable(), 1);
			}
		}

	}

	void setLabel(String newText) {
		System.out.println(newText);
	}

	public String getFilename() {
		return filename;
	}

	public static void fitToContentWidth(final JTable table, final int column) {
		int width = 0;
		for (int row = 0; row < table.getRowCount(); ++row) {
			final Object cellValue = table.getValueAt(row, column);
			final TableCellRenderer renderer = table.getCellRenderer(row,
					column);
			final Component comp = renderer.getTableCellRendererComponent(
					table, cellValue, false, false, row, column);
			width = Math.max(width, comp.getPreferredSize().width);
		}
		final TableColumn tc = table.getColumn(table.getColumnName(column));
		width += table.getIntercellSpacing().width * 2;
		tc.setPreferredWidth(width * 2);
		tc.setMinWidth(width * 2);
	}
}
