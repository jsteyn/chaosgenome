package com.jannetta.chaosgenome.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

import com.jannetta.chaosgenome.controller.Helpers;
import com.jannetta.chaosgenome.model.Coord;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class DrawPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int numberOfVertices = 3;
	private long numberOfPoints = 1000000;
	private String filename = null;
	private float[][] matrix;
	private Coord[] vertex;
	private float maxgreyval;
	private float maximum = 0;
	private float minimum = 255;
	private boolean draw = false;
	float maxx = 0;
	float maxy = 0;
	float minx = getWidth();
	float miny = getHeight();

	public DrawPanel() {
		super();
		setVisible(true);
		// ADDED
		setAutoscrolls(true);
	}

	@Override
	public void paintComponent(Graphics g) {
		maxx = 0;
		maxy = 0;
		minx = getWidth();
		miny = getHeight();
		if (draw) {
			if (matrix != null && vertex != null) {
				int size = matrix[0].length;
				super.paintComponent(g);
				g.setColor(Color.BLACK);
				g.clearRect(0, 0, getWidth(), getHeight());

				// draw outline of figure
				for (int i = 0; i < numberOfVertices - 1; i++) {
					//System.out.println("vertex: " + i);
					if (vertex[i].getX() > maxx)
						maxx = vertex[i].getX();
					if (vertex[i].getY() > maxy)
						maxy = vertex[i].getY();
					if (vertex[i].getX() < minx)
						minx = vertex[i].getX();
					if (vertex[i].getY() < miny)
						miny = vertex[i].getY();
					g.drawLine((int) vertex[i].getX(), (int) vertex[i].getY(),
							(int) vertex[i + 1].getX(),
							(int) vertex[i + 1].getY());
				}
				// Close figure
				g.drawLine((int) vertex[numberOfVertices - 1].getX(),
						(int) vertex[numberOfVertices - 1].getY(),
						(int) vertex[0].getX(), (int) vertex[0].getY());

				// draw figure
				int pointsinpath=0;
				int pointsnotinpath=0;
				for (int i = 0; i < size; i++) {
					for (int j = 0; j < size; j++) {
						/*
						 * If the point to be plotted is not inside the polygon then
						 * just make it white.
						 */
						if (!Helpers.isPointInPath2(i, j, vertex)) {
							g.setColor(new Color(255, 255, 255));
							g.drawLine(i, j, i, j);
							pointsnotinpath++;
						/*
						 * If the point is inside the polygon, calculate its grey value
						 */
						} else {
							pointsinpath++;
							int grey = 255;
							// Do some jiggling to display a decent image
							// There are 256 shades of grey, 0 to 255
							// If less than 255 shades of grey is used
							// we divide the maximum value in the matrix by the
							// number of grey values, i.e. div.
							// We divide the value from the matrix by div and
							// multiply it by the number of grey values.
							// This divides the grey range into equal sections
							// giving us some contrast between the values.
							// We add 1 to avoid division by zero because
							// we then divide 255 by the section value.
							if (this.maxgreyval < 255) {
								int div = (int) Math.floor((255/this.maxgreyval));
								//int section = ((int) ((int) (matrix[i][j] / div) * this.maxgreyval)) + 1;
								//grey = 255 / section;
								int a = (int) this.maxgreyval;
								int b = 0;
								int A = (int) this.minimum;
								int B = (int) this.maximum;
								grey = (int) (a+(((matrix[i][j]-A)*(b-a))/(B-A))) * div;
								//System.out.println(div + ", " + grey);
							} else
								grey = (int) (this.maxgreyval - (matrix[i][j]
										/ maximum * this.maxgreyval));
							g.setColor(new Color(grey, grey, grey));
							g.drawLine(i, j, i, j);
						}
					}
				}
				System.out.println("Points plotted: " + pointsinpath);
				System.out.println("Points not plotted: " + pointsnotinpath);
			} else {
				if (matrix == null)
					System.out.println("matrix empty");
				if (vertex == null)
					System.out.println("vertex empty");
			}
		}
	}

	public void setDraw(boolean draw) {
		this.draw = draw;
	}

	public int getNumberOfVertices() {
		return numberOfVertices;
	}

	public void setNumberOfVertices(int numberOfVertices) {
		this.numberOfVertices = numberOfVertices;
	}

	public long getNumberOfPoints() {
		return numberOfPoints;
	}

	public void setNumberOfPoints(long numberOfPoints) {
		this.numberOfPoints = numberOfPoints;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Convert matrix value to range from 0 to grey value and draw image
	 * 
	 * @param matrix
	 *            matrix containing the image
	 * @param vertex
	 *            vertices of the image
	 * @param grey
	 *            maximum grey scale value
	 */
	public void drawMatrix(float[][] matrix, Coord[] vertex, int maxgreyvalue) {
		this.vertex = vertex;
		this.matrix = matrix;
		this.maximum = 0;
		this.minimum = 255;
		/*
		 * Get the largest value in the matrix. This will be used to normalise
		 * the values to between 0 and the maxgreyval
		 */
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] > maximum)
					maximum = matrix[i][j];
				if (matrix[i][j] < minimum)
					minimum = matrix[i][j];
			}
		}
		setPreferredSize(new Dimension(matrix[0].length, matrix[0].length));
		this.maxgreyval = maxgreyvalue;
		this.draw = true;
		repaint();
	}

	public void saveImage() {
		JFileChooser fileChooser = new JFileChooser();
		if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			// save to file
			BufferedImage image = new BufferedImage(matrix[0].length, matrix[0].length,
					BufferedImage.TYPE_INT_RGB);
			Graphics g = image.createGraphics();
			paint(g);
			try {
				ImageIO.write(image, "png", file);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			System.out.println("Open command cancelled by user.");
		}
	}

	/**
	 * Get the vertices that make up the polygon on this panel
	 * 
	 * @return vertices as an array of Coord
	 */
	public Coord[] getVertices() {
		return vertex;
	}

}
