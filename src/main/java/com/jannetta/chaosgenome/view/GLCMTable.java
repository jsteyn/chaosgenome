package com.jannetta.chaosgenome.view;

import java.awt.Dimension;
import javax.swing.JTable;
import com.jannetta.chaosgenome.model.GLCMTableModel;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class GLCMTable extends JTable{

	/**
	 * Table for displaying VSD_data
	 * 
	 */
	private static final long serialVersionUID = -2308670495144915487L;
	static GLCMTableModel glcm_model = new GLCMTableModel();

	public GLCMTable() {
		super(glcm_model);
		glcm_model.addTableModelListener(this);
		setAutoCreateColumnsFromModel(true);
		createDefaultColumnsFromModel();
		setAutoscrolls(true);
		setFillsViewportHeight(true);
		//setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
		
		System.out.println("Viewport size: " + getPreferredScrollableViewportSize());
	}
	
	public void fireTableDataChanged() {
		setPreferredSize(new Dimension(getWidth(),getHeight()));
		glcm_model.fireTableStructureChanged();
	}
	
}
