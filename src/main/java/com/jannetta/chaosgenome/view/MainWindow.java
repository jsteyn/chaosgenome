package com.jannetta.chaosgenome.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class MainWindow extends JFrame {

	private static final long serialVersionUID = 1L;
	private MainMenuBar mainmenubar = new MainMenuBar(this);
	private JSplitPane splitPane;
	private JTabbedPane tabbedPane = new JTabbedPane();
	private JTextArea nucleotides = new JTextArea();
	private JTextArea conversion = new JTextArea();
	private DrawPanel drawpanel =new DrawPanel();
	private PlainDrawPanel plainpanel = new PlainDrawPanel();
	private GLCMTable glcmtable = new GLCMTable();
	
	public MainWindow() {
		this.setTitle("Chaos Game Genome Representation");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(800, 600));
		this.setLayout(new BorderLayout());
		this.setJMenuBar(mainmenubar);
		
		JScrollPane sp_nuc = new JScrollPane(nucleotides);
		JScrollPane sp_con = new JScrollPane(conversion);
		JScrollPane sp_drw = new JScrollPane(drawpanel);
		JScrollPane sp_pdrw = new JScrollPane(plainpanel);
		JScrollPane sp_glcm = new JScrollPane(glcmtable,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		drawpanel.setPreferredSize(new Dimension(800, 600));
		plainpanel.setPreferredSize(new Dimension(800, 600));
		glcmtable.setPreferredSize(new Dimension(8000,6000));
		
		Dimension minimumSize = new Dimension(100, 50);
		sp_nuc.setMinimumSize(minimumSize);
		sp_con.setMinimumSize(minimumSize);
		sp_glcm.setMinimumSize(minimumSize);
        
		// Split pane for displaying read file and converted file
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, sp_nuc,
				sp_con);
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation(400);
		splitPane.setPreferredSize(new Dimension(400, 200));
		
		tabbedPane.addTab("Files", null, splitPane, "Files");
		tabbedPane.addTab("Images", null, sp_drw, BorderLayout.CENTER);
		tabbedPane.addTab("DrawPanel", null, sp_pdrw, BorderLayout.CENTER);
		tabbedPane.addTab("GLCM", null, sp_glcm, BorderLayout.CENTER);
		
		getContentPane().add(tabbedPane);
		nucleotides.setText("");
		conversion.setText("");
		setVisible(true);
		pack();
	}

	/**
	 * @param text
	 * Append text to nucleotide text area
	 */
	public void setNucleotideText(String text) {
		nucleotides.append(text);
	}
	
	/**
	 * @param text
	 * Append text to converted text area
	 */
	public void setConvertText(String text) {
		conversion.append(text);
	}
	
	/**
	 * @return
	 * return JTextArea containing nucleotides
	 */
	public JTextArea getNucTextArea() {
		return nucleotides;
	}

	/**
	 * @return
	 * return JTextArea containing converted nucleotides
	 */
	public JTextArea getConTextArea() {
		return conversion;
	}
	
	public DrawPanel getDrawPanel() {
		return drawpanel;
	}
	
	public PlainDrawPanel getPlainDrawPanel() {
		return plainpanel;
		
	}
	
	public GLCMTable getGLCMTable() {
		return glcmtable;
	}
}
