package com.jannetta.chaosgenome.model;

/**
 * @author Jannetta S Steyn
 * @date 2014
 *         Class converting codons to a single character respresentation. First
 *         the letters of the alphabet is used as capitals, then lower case,
 *         then the digits 0 to 9 and then the / character.
 *
 */
public class Codons extends Coding {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 */
	public Codons() {
		put("TTT", "A");
		put("TTC", "B");
		put("TTA", "C");
		put("TTG", "D");
		put("CTT", "E");
		put("CTC", "F");
		put("CTA", "G");
		put("CTG", "H");
		put("ATT", "I");
		put("ATC", "J");
		put("ATA", "K");
		put("ATG", "L");
		put("GTT", "M");
		put("GTC", "N");
		put("GTA", "O");
		put("GTG", "P");

		put("TCT", "Q");
		put("TCC", "R");
		put("TCA", "S");
		put("TCG", "T");
		put("CCT", "U");
		put("CCC", "V");
		put("CCA", "W");
		put("CCG", "X");
		put("ACT", "Y");
		put("ACC", "Z");
		put("ACA", "a");
		put("ACG", "b");
		put("GCT", "c");
		put("GCC", "d");
		put("GCA", "e");
		put("GCG", "f");

		put("TAT", "g");
		put("TAC", "h");
		put("TAA", "i");
		put("TAG", "j");
		put("CAT", "k");
		put("CAC", "l");
		put("CAA", "m");
		put("CAG", "n");
		put("AAT", "o");
		put("AAC", "p");
		put("AAA", "q");
		put("AAG", "r");
		put("GAT", "s");
		put("GAC", "t");
		put("GAA", "u");
		put("GAG", "v");

		put("TGT", "w");
		put("TGC", "x");
		put("TGA", "y");
		put("TGG", "z");
		put("CGT", "0");
		put("CGC", "1");
		put("CGA", "2");
		put("CGG", "3");
		put("AGT", "4");
		put("AGC", "5");
		put("AGA", "6");
		put("AGG", "7");
		put("GGT", "8");
		put("GGC", "9");
		put("GGA", "0");
		put("GGG", "/");
	}

}
