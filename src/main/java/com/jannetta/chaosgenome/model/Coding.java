package com.jannetta.chaosgenome.model;

import java.util.Hashtable;

/**
 * @author Jannetta S Steyn
 * @date July 2014
 * 
 * Abstract class for use of different amino acid encodings
 *
 */
public abstract class Coding extends Hashtable<String, String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
