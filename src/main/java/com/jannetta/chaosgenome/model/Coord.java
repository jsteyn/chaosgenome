package com.jannetta.chaosgenome.model;

/**
 * @author Jannetta Steyn
 * @date July 2014
 * 
 *       Class for holding co-ordinates of the points of the image to be plotted
 *
 */
public class Coord {

	private float x;
	private float y;

	/**
	 * @param x
	 * @param y
	 * 
	 * Instantiate class with x and y co-ordinates
	 */
	public Coord(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * @return x value
	 * 
	 * Return the x value of the point
	 */
	public float getX() {
		return x;
	}

	/**
	 * @return y value
	 * 
	 * Return the y value of the point
	 */
	public float getY() {
		return y;
	}

}
