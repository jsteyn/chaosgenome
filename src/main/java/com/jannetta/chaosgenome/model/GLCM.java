package com.jannetta.chaosgenome.model;

import com.jannetta.chaosgenome.controller.Helpers;

/**
 * @author Jannetta S Steyn
 * @date 2014
 * 
 *         This class holds a matrix with the gray level co-occurrences in. The
 *         gray scale is 0 to 255. So the matrix is of dimension [256][256]
 *
 */
public class GLCM {

	/**
	 * Gray level co-occurrence matrix
	 */
	static int size;
	private static long[][] glcmatrix;
	private static int[][] imagematrix;
	private static Coord[] vertices;
	private static float maximum = 0;
	private static int maxgreylevel = 0;

	/**
	 * Constructor: Initialise matrix
	 */
	/**
	 * @param size
	 *            - the size of the glcm, thus the maximum value that any cell
	 *            in the imagematrix will contain
	 * @param imagematrix
	 *            - the matrix containing the image of the polygon
	 * @param vertices
	 *            - the vertices that comprise the polygon
	 */
	public GLCM() {
	}

	/**
	 * Initialise the GLCM
	 * 
	 * @param imagematrix
	 * @param vertices
	 * @param maxgreylevel
	 */
	public static void initGLCM(int[][] imagematrix, Coord[] vertices,
			int maxgreylevel) {
		GLCM.size = maxgreylevel;
		// why did I add 1????
		GLCM.glcmatrix = new long[maxgreylevel + 1][maxgreylevel + 1];
		// GLCM.glcmatrix = new long[maxgreylevel][maxgreylevel];
		GLCM.maxgreylevel = maxgreylevel;
		// initialise matrix;
		for (int row = 0; row < maxgreylevel; row++) {
			for (int col = 0; col < maxgreylevel; col++) {
				glcmatrix[row][col] = 0;
			}
		}
		// Get maximum value in imagematrix
		GLCM.maximum = 0;
		for (int i = 0; i < imagematrix.length; i++) {
			for (int j = 0; j < imagematrix.length; j++) {
				if (imagematrix[i][j] > GLCM.maximum)
					GLCM.maximum = imagematrix[i][j];
			}
		}
		GLCM.imagematrix = imagematrix;
		GLCM.vertices = vertices;
		System.out.println("Size of imagematrix: " + GLCM.imagematrix.length);
		System.out.println("Number of vertices: " + GLCM.vertices.length);
		System.out.println("Size of GLCM: " + GLCM.maxgreylevel);
	}

	public long[][] getMatrix() {
		return glcmatrix;
	}

	public void setImageMatrix(int[][] imagematrix) {
		GLCM.imagematrix = imagematrix;
	}

	/**
	 * Calculate the GLCM For each point check that the point and its follower
	 * is in the polygon. If they are increment the where the two grey scale
	 * values intersect in the GLCM.
	 */
	public static void calcGLCM() {
		int distance = 1;
		System.out.println("Calculating GLCM");
		System.out.println("Size: " + GLCM.size);
		System.out.println("Maximum: " + GLCM.maximum);
		System.out.println("Max Grey level: " + GLCM.maxgreylevel);
		/*
		 * For each point and its following point in the imagematrix check
		 * whether it falls within the polygon. If it does increment the GLCM.
		 */
		for (int row = 0; row < imagematrix.length; row++) {
			for (int col = 0; col < imagematrix.length - distance; col++) {
				if (Helpers.isPointInPath2(row, col, GLCM.vertices)
						&& Helpers.isPointInPath2(row, col + distance,
								GLCM.vertices)) {
					//int yval = (int) (imagematrix[row][col] / GLCM.maximum * (GLCM.maxgreylevel));
					//int xval = (int) (imagematrix[row][col + distance]
					//		/ GLCM.maximum * (GLCM.maxgreylevel));
					int yval = (int) (((imagematrix[row][col])*(GLCM.maxgreylevel)/GLCM.maximum));
					int xval = (int) (((imagematrix[row][col] + distance)*(GLCM.maxgreylevel)/GLCM.maximum));
					incCell(xval, yval);
				}
			}
		}
		for (int row = 0; row < glcmatrix.length; row++) {
			for (int col = 0; col < glcmatrix.length; col++) {
				System.out.print(glcmatrix[row][col] + "\t");
			}
			System.out.println("");
		}

	}

	public static void incCell(int row, int col) {
		glcmatrix[row][col]++;
	}

	public long getCell(int row, int col) {
		return glcmatrix[row][col];
	}

	public int getSize() {
		return size;
	}

}
