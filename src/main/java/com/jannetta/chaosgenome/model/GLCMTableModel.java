package com.jannetta.chaosgenome.model;
import javax.swing.table.AbstractTableModel;

/**
 * @author Jannetta S. Steyn
 * @date 2014
 *
 */
public class GLCMTableModel extends AbstractTableModel  {

	float[][] imagematrix;
	Coord[] vertices;
	/**
	 * 
	 */
	private static final long serialVersionUID = 737198854514334060L;
	GLCM glcm = new GLCM();
	
	@Override
	public int getColumnCount() {
		return glcm.getSize(); //FileReader.getColCount();
	}

	@Override
	public int getRowCount() {
		return glcm.getSize(); //FileReader.getRowCount();
	}

	@Override
	public String getColumnName(int col) {
		return "" + col; //FileReader.getHeaders()[col];
	}

	@Override
	public Object getValueAt(int row, int col) {
//		return FileReader.getData()[row][col];//data[row][col]; // Inefficient
		return glcm.getCell(col, row); //FileReader.getDataAt(row, col);//data[row][col];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return getValueAt(0, columnIndex).getClass();
	}


	@Override
	public boolean isCellEditable(int row, int column) {
		if (column == 0 || column == 1) {
			return false;
		} else {
			return true;
		}
	}

}

